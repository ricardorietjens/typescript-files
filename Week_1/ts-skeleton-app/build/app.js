class Ship {
    constructor(name, color, cannons, distanceTravelled, health) {
        this.name = name;
        this.color = color;
        this.cannons = cannons;
        this.distanceTravelled = distanceTravelled;
        this.health = health;
    }
    addDistanceTravelled() {
        this.distanceTravelled++;
    }
    substractDamage() {
        this.health--;
    }
    getDamage() {
        this.substractDamage();
    }
    shoot() {
        console.log("Piew! Piew!");
    }
    move() {
        console.log("I'm moving!");
        this.addDistanceTravelled();
        console.log(this.distanceTravelled);
    }
    showHealth() {
        console.log(`Remaining ${this.health} lives!`);
    }
    getHealth() {
        return this.health;
    }
    getName() {
        return this.name;
    }
}
let spaceShip = new Ship("USS Enterprise", "Grijs", 35, 10000, 5);
spaceShip.move();
spaceShip.move();
spaceShip.shoot();
spaceShip.showHealth();
class Asteroid {
    constructor(number, name, xPosition, yPosition, diameter) {
        this.number = number;
        this.name = name;
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.diameter = diameter;
    }
    showPosition() {
        console.log(`Asteroid ${this.number} (${this.name}) staat op een X van ${this.xPosition} en een Y van ${this.yPosition}
        en heeft een diameter van ${this.diameter}`);
    }
    hitSpaceShip() {
        console.log(`Auch that hurts!`);
    }
    moveDuringFlight() {
        this.xPosition++;
        this.yPosition++;
    }
    fly() {
        this.moveDuringFlight();
        console.log(`Asteroid ${this.name} moved to X= ${this.xPosition} and Y= ${this.yPosition}.`);
    }
    damageAsteroid() {
        let newDiameter = this.diameter / 2;
        this.diameter = newDiameter;
        console.log(`The new diameter of ${this.name} is ${this.diameter}`);
        if (this.diameter < 3) {
            this.removeAsteroid();
        }
    }
    damageSpaceShip() {
        spaceShip.getDamage();
        let nameOfSpaceShip = spaceShip.getName();
        let healthOfSpaceShip = spaceShip.getHealth();
        console.log(`That hurts! Now ${nameOfSpaceShip} has ${healthOfSpaceShip} health points left...`);
        this.damageAsteroid();
    }
    removeAsteroid() {
        console.log(`${this.name} will now be deleted...`);
        delete this.number;
        delete this.name;
        delete this.diameter;
        delete this.xPosition;
        delete this.yPosition;
    }
}
let Halley = new Asteroid(1, 'Halley', 38, 72, 16);
let Encke = new Asteroid(2, 'Encke', 87, 29, 11);
let Ikeya_Seki = new Asteroid(3, 'Ikeya_Seki', 43, 61, 14);
Halley.showPosition();
Halley.fly();
Halley.fly();
Halley.fly();
Halley.hitSpaceShip();
Halley.damageSpaceShip();
Halley.damageSpaceShip();
Halley.damageSpaceShip();
class Student {
    constructor(name, age, student) {
        this.name = name;
        this.age = age;
        this.student = student;
    }
    showInfo() {
        return this.name + " is " + this.age + " jaar oud en heeft studentnummer " + this.student + ".";
    }
}
let Ricardo = new Student("Ricardo", 17, 77848);
document.body.appendChild(document.createTextNode(Ricardo.showInfo()));
//# sourceMappingURL=app.js.map