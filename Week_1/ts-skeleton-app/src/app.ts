// yarn watch in cmd om realtime te compilen van ts naar js
// Een klasse is een mal, een object is een ingevulde mal (uniek)

// Een class uit je classdiagram
class Ship {
    // Attributes
    private name: string;
    public color: string;
    private cannons: number;
    private distanceTravelled: number;
    public health: number;

    public constructor(
        name: string,
        color: string,
        cannons: number,
        distanceTravelled: number,
        health: number
    ) {
        this.name = name;
        this.color = color;
        this.cannons = cannons;
        this.distanceTravelled = distanceTravelled;
        this.health = health;
    }

    // Vervangen door constructor (spaceShip.cannons = 35 --> spaceShip.setCannons(35);)

    // public setName(amount: string) {
    //     this.name = amount
    // }

    // public setCannons(amount: number) {
    //     this.cannons = amount
    // }

    // public setDistanceTravelled(amount: number) {
    //     this.distanceTravelled = amount
    // }

    // Functie om één kilometer toe te voegen aan de teller
    private addDistanceTravelled() {
        this.distanceTravelled++;
    }

    // Functie om één leven er af te halen
    private substractDamage() {
        this.health--
    }
 
    // Functie om één leven er af te halen aanroepen
    public getDamage() {
        this.substractDamage();
    }

    // Functie om te schieten
    public shoot() {
        console.log("Piew! Piew!")
    }

    // Functie om te bewegen en te tellen hoeveel
    public move() {
        console.log("I'm moving!")
        this.addDistanceTravelled();
        console.log(this.distanceTravelled);
    }

    // Functie om te laten zien wat het nieuwe aantal levens is
    public showHealth() {
        console.log(`Remaining ${this.health} lives!`)
    }

    // Funtie om de health van het SpaceShip te returnen
    public getHealth() {
        return this.health;
    }

    // Functie om de naam van het SpaceShip te returnen
    public getName() {
        return this.name
    }
}

// Object(en)
let spaceShip: Ship = new Ship("USS Enterprise", "Grijs", 35, 10000, 5);

// Functies aanropen
spaceShip.move();
spaceShip.move();
spaceShip.shoot();
spaceShip.showHealth();

// Nieuwe class voor asteroid
class Asteroid {
    private number: number;
    private name: string;
    private xPosition: number;
    private yPosition: number;
    private diameter: number;

    public constructor(
        number: number,
        name: string,
        xPosition: number,
        yPosition: number,
        diameter: number
    ) {
        this.number = number;
        this.name = name;
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.diameter = diameter;
    }

    // Functie voor het aangeven wat er in de console moet komen te staan over de gegevens van de asteroid
    public showPosition() {
        console.log(`Asteroid ${this.number} (${this.name}) staat op een X van ${this.xPosition} en een Y van ${this.yPosition}
        en heeft een diameter van ${this.diameter}`);
    }

    // Weer te geven tekst bij aanraken SpaceShip
    public hitSpaceShip() {
        console.log(`Auch that hurts!`)
    }

    // Nieuwe coördinaten maken om heen te vliegen
    private moveDuringFlight() {
        this.xPosition++;
        this.yPosition++;
    }

    // functie om weer te geven wat de nieuwe positie is
    public fly() {
        this.moveDuringFlight();
        console.log(`Asteroid ${this.name} moved to X= ${this.xPosition} and Y= ${this.yPosition}.`)
    }

   
    // Elke keer dat de asteroid wordt geraakt wordt de asteroid in 2 gesplitst
    private damageAsteroid() {
        let newDiameter = this.diameter / 2;
        this.diameter = newDiameter;
        console.log(`The new diameter of ${this.name} is ${this.diameter}`);

        // Wanneer de diameter kleiner is dan 3 wordt de asteroid verwijderd
        if(this.diameter < 3) {
            this.removeAsteroid();
        }
    }

    // Functie om één punt van de health af te trekken en dit weer te geven
    public damageSpaceShip() {
        spaceShip.getDamage();
        let nameOfSpaceShip = spaceShip.getName();
        let healthOfSpaceShip = spaceShip.getHealth();
        console.log(`That hurts! Now ${nameOfSpaceShip} has ${healthOfSpaceShip} health points left...`);
        this.damageAsteroid();
    }

     // Verwijderen van asteroid, dit ook weergeven
     private removeAsteroid() {
        console.log(`${this.name} will now be deleted...`)
        delete this.number;
        delete this.name;
        delete this.diameter;
        delete this.xPosition;
        delete this.yPosition;
    }
}

// Objecten (+ Gegevens die in de string geplaatst moeten worden)
let Halley: Asteroid = new Asteroid(1, 'Halley', 38, 72, 16);
let Encke: Asteroid = new Asteroid(2, 'Encke', 87, 29, 11);
let Ikeya_Seki: Asteroid = new Asteroid(3, 'Ikeya_Seki', 43, 61, 14);

// Functies aanroepen
Halley.showPosition();
Halley.fly();
Halley.fly();
Halley.fly();
Halley.hitSpaceShip();
Halley.damageSpaceShip();
Halley.damageSpaceShip();
Halley.damageSpaceShip();

// Encke.showPosition();
// Encke.hitSpaceShip();
// Encke.fly();
// Encke.damageSpaceShip()

// Ikeya_Seki.showPosition();
// Ikeya_Seki.hitSpaceShip();
// Ikeya_Seki.fly();
// Ikeya_Seki.damageSpaceShip();


// Extra opdracht studentgegevens bijspijkerles
class Student {
    // Moeten public zijn om de waarde uit de string te kunnen printen
    public name: string;
    public age: number;
    public student: number;

    public constructor(
        name: string,
        age: number,
        student: number
    ) {
        this.name = name;
        this.age = age;
        this.student = student;
    }

    // Fucntie voor de weer te geven tekst
    public showInfo() {
        return this.name + " is " + this.age + " jaar oud en heeft studentnummer " + this.student + "."
    }
}

// Object(en)
let Ricardo: Student = new Student("Ricardo", 17, 77848);
document.body.appendChild(document.createTextNode(Ricardo.showInfo()))